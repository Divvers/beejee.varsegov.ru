<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<link rel="stylesheet" href="css/app.css">
		<title>BeeJee</title>
	</head>
	<body>
		<div class="container-fluid">
			<h1>Тестовое задание BeeJee</h1>
			<p>&copy; Дмитрий Варсегов, 14.12.2019</p>
			<p>
				<a class="btn btn-primary" href="/">Все задачи</a>
				<a class="btn btn-primary" href="/?p=task">Создать задачу</a>
				<?php if (isset($_SESSION['userID'])) { ?>
					<a class="btn btn-danger" href="/?p=logout">Выход</a>
				<?php } else { ?>
					<a class="btn btn-success" href="/?p=authForm">Вход</a>
				<?php } ?>
			</p>
