<?php if (isset($error)) { ?>
	<p class="alert alert-danger"><?php echo $error; ?></p>
<?php } ?>
<h2>Авторизация</h2>
<form method="post" action="/?p=auth">
	<p>
		<label>Имя пользователя<br />
			<input type="text" name="name" maxlength="255" />
		</label>
	</p>
	<p>
		<label>Пароль<br />
			<input type="password" name="password" maxlength="255" />
		</label>
	</p>
	<p>
		<input type="submit" value="Войти" />
	</p>
</form>
