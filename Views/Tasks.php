<p>Сортировка:
	<select name="sort" onchange="location.href = '/?<?php if ($page > 1) echo 'page=' . $page . '&'; ?>sort=' + this.options[this.selectedIndex].value;">
		<?php foreach ($sortValues as $k => $v) { ?>
			<option value="<?php echo $k; ?>" <?php if ($k == $_SESSION['sort']) echo 'selected'; ?>><?php echo $v; ?></option>
		<?php } ?>
	</select>
</p>
<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Имя пользователя</th>
			<th scope="col">E-mail</th>
			<th scope="col">Текст задачи</th>
			<th scope="col">Статус</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($tasks as $t) { ?>
			<tr class="task" onclick="location.href = '/?p=task&id=<?php echo $t['id']; ?>'">
				<td><?php echo $t['id']; ?></td>
				<td><?php echo htmlspecialchars($t['username']); ?></td>
				<td><?php echo htmlspecialchars($t['email']); ?></td>
				<td><?php echo htmlspecialchars($t['description']); ?></td>
				<td>
					<?php if ($t['done']) echo 'Выполнено<br />' ?>
					<?php if ($t['changed']) echo 'Отредактировано администратором' ?>
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<?php if ($pagesCount > 1) { ?>
	<p>Страницы: 
		<?php for ($i = 1; $i <= $pagesCount; $i++) { ?>
			<a class="btn btn-light" href="/?page=<?php echo $i; ?>"><?php echo $i; ?></a>
		<?php } ?>
	</p>
<?php } ?>
