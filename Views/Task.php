<h2>
	<?php
	if ($task['id'] == 0) echo 'Новая задача';
	else echo 'Задача #' . $task['id'];
	?>
</h2>
<?php if (isset($error)) { ?>
	<p class="alert alert-danger"><?php echo $error; ?></p>
<?php } ?>
<?php
if ($_GET['saved'] == 1) $msg = 'Задача создана';
else if ($_GET['saved'] == 2) $msg = 'Изменения сохранены';
?>
<?php if (isset($msg)) { ?>
	<p class="alert alert-success"><?php echo $msg; ?></p>
<?php } ?>
<form method="post" action="/?p=saveTask">
	<input type="hidden" name="id" value="<?php echo $task['id']; ?>" />
	<p>
		<label>Имя пользователя<br />
			<input type="text" name="username" maxlength="255" value="<?php echo $task['username']; ?>" />
		</label>
	</p>
	<p>
		<label>E-mail<br />
			<input type="text" name="email" maxlength="255" value="<?php echo $task['email']; ?>" />
		</label>
	</p>
	<p>
		<label>Текст задачи<br />
			<textarea name="description" ><?php echo $task['description']; ?></textarea>
		</label>
	</p>
	<?php if ($task['id'] != 0) { ?>
		<p>
			<label>
				<input type="checkbox" name="done" value="1" <?php if ($task['done']) echo 'checked'; ?> />
				Выполнено
			</label>
		</p>
	<?php } ?>
	<p>
		<input type="submit" value="Сохранить" />
	</p>
</form>
	