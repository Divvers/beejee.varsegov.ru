<?php

class App extends BaseController
{

	public function tasks()
	{
		$page = intval($_GET['page']);
		$sort = $_GET['sort'];
		$this->loadmodel('Task');
		$sortValues = $this->Task->sortValues();
		if (isset($sort) && array_key_exists($sort, $sortValues)) {
			$_SESSION['sort'] = $sort;
		}
		$tasks = $this->Task->getTasks($page);
		$pagesCount = $this->Task->getPagesCount();
		$this->loadview('Tasks', ['tasks' => $tasks, 'page' => $page, 'pagesCount' => $pagesCount, 'sortValues' => $sortValues]);
	}

	public function task($id, $msg)
	{
		if (!isset($id)) $id = intval($_GET['id']);
		$this->loadmodel('Task');
		$task = $this->Task->getTask($id);
		$this->loadview('Task', ['task' => $task, 'msg' => $msg]);
	}

	public function saveTask()
	{
		$id = intval($_POST['id']);
		$this->loadmodel('Task');
		$res = $this->Task->saveTask();
		if (isset($res['authError'])) {
			$this->loadview('AuthForm', ['error' => $res['authError']]);
		} else if (isset($res['error'])) {
			$this->loadview('Task', ['task' => $_POST, 'error' => $res['error']]);
		} else {
			if ($id == 0) $saved = 1;
			else $saved = 2;
			header('Location: http://' . $_SERVER['HTTP_HOST'] . '/?p=task&id=' . $res . '&saved=' . $saved);
		}
	}

	public function authForm()
	{
		$this->loadview('AuthForm');
	}

	public function auth()
	{
		$this->loadmodel('User');
		$res = $this->User->auth();
		if (isset($res['error'])) {
			$this->loadview('AuthForm', $res);
		} else {
			header('Location: http://' . $_SERVER['HTTP_HOST'] . '/');
		}
	}

	public function logout()
	{
		$this->loadmodel('User');
		$this->User->logout();
		header('Location: http://' . $_SERVER['HTTP_HOST'] . '/');
	}

}
