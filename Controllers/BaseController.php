<?php

class BaseController
{

	public function loadview($view, $data)
	{
		extract($data);
		include 'Views/Header.php';
		include 'Views/' . $view . '.php';
		include 'Views/Footer.php';
	}

	public function loadmodel($model)
	{
		require_once 'Models/' . $model . '.php';
		if (!isset($this->$model)) $this->$model = new $model;
	}

}
