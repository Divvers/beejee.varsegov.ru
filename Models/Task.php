<?php

class Task extends BaseModel
{

	const TASKS_PER_PAGE = 3;

	public function getTask($id)
	{
		if ($id == 0) return [];
		$sql = 'SELECT * FROM task WHERE id=? LIMIT 1';
		$result = $this->db->prepare($sql);
		$result->execute([$id]);
		$tdata = $result->fetchAll();
		return $tdata[0];
	}

	public function saveTask()
	{
		$id = intval($_POST['id']);
		if ($id != 0 && !isset($_SESSION['userID'])) {
			return ['authError' => 'Для редактирования задач необходимо авторизоваться'];
		}
		$fields = [
			'username',
			'email',
			'description'
		];
		$values = [];
		foreach ($fields as $f) {
			if (!isset($_POST[$f]) || $_POST[$f] == '') {
				return ['error' => 'Заполните все поля'];
			}
			$values[] = $_POST[$f];
		}
		if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			return ['error' => 'Некорректный e-mail'];
		}
		if ($id == 0) {
			$sql = 'INSERT task(username,email,description) VALUES (?,?,?)';
			$result = $this->db->prepare($sql);
			$result->execute($values);
			$id = $this->db->lastInsertId();
		} else {
			$values[] = intval($_POST['done']);
			$task = $this->getTask($id);
			if ($task['description'] == $_POST['description']) {
				$values[] = $task['changed']; // not changed
			} else {
				$values[] = 1; // changed
			}
			$sql = 'UPDATE task SET username=?,email=?,description=?,done=?,changed=? WHERE id=' . $id;
			$result = $this->db->prepare($sql);
			$result->execute($values);
		}
		return $id;
	}

	public function getTasks($page = 1)
	{
		if ($page < 1) $page = 1;
		$start = ($page - 1) * self::TASKS_PER_PAGE;
		$order = '';
		if (isset($_SESSION['sort'])) $order = 'ORDER BY ' . $_SESSION['sort'];
		$sql = 'SELECT * FROM task ' . $order . ' LIMIT ' . $start . ',' . self::TASKS_PER_PAGE . ' ';
		$result = $this->db->prepare($sql);
		$result->execute();
		$tdata = $result->fetchAll();
		return $tdata;
	}

	public function getPagesCount()
	{
		$sql = 'SELECT COUNT(id) AS cnt FROM task';
		$result = $this->db->prepare($sql);
		$result->execute();
		$tdata = $result->fetchAll();
		$cnt = $tdata[0]['cnt'];
		$pages = ceil($cnt / self::TASKS_PER_PAGE);
		return $pages;
	}

	public function sortValues()
	{
		return [
			'username' => 'Имя пользователя по возрастанию',
			'username desc' => 'Имя пользователя по убыванию',
			'email' => 'E-mail по возрастанию',
			'email desc' => 'E-mail по убыванию',
			'done,changed' => 'Статус по возрастанию',
			'done desc,changed desc' => 'Статус по убыванию'
		];
	}

}
