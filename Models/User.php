<?php

class User extends BaseModel
{

	public function auth()
	{
		if ($_POST['name'] == '' || $_POST['password'] == '') {
			return ['error' => 'Заполните поля'];
		}
		if ($_POST['name'] != 'admin' || $_POST['password'] != '123') {
			return ['error' => 'Неправильное имя пользователя или пароль'];
		}
		$_SESSION['userID'] = 1;
		return true;
	}

	public function logout()
	{
		unset($_SESSION['userID']);
	}

}
