<?php

error_reporting(E_ALL);
session_start();
try {
	$db = new PDO('mysql:host=localhost;dbname=vh121014_beejee;charset=utf8', 'vh121014_beejee', '9F9k8W6c');
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
} catch (Exception $e) {
	die('Ошибка БД');
}
require 'Controllers/BaseController.php';
require 'Models/BaseModel.php';
require 'Controllers/App.php';
$app = new App;
$p = filter_input(INPUT_GET, 'p');
if (method_exists($app, $p)) $app->$p();
else $app->tasks();
